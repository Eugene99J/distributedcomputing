# Рабочий - забирает картинки с сервера, распознает их и отдает обратно
import socket
import sys
import dlib
import cv2
import logging
import util
import json
import sys
import numpy as np

file_log = logging.FileHandler("client.log")
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out),
                    format='[%(asctime)s | %(levelname)s | CLIENT]: %(message)s',
                    datefmt='%m.%d.%Y %H:%M:%S',
                    level=logging.INFO)

# main_port
scheduler_port = 9090
scheduler_addr = '192.168.88.252'
server_addr = '192.168.88.252'

# Рабочие не спят, работают 24/7
# TODO сделать местный kill
def start_job():
    while True:
        sock = socket.socket()
        # Хотим выделенный порт для работы
        port = get_port(sock)
        sock.close()

        # Хотим матрицы
        sock = socket.socket()
        matrices = get_matrices(sock, port)

        # Распознаем
        logging.info("Start detect...")
        matrix = multiply(matrices)
        logging.info("Computed!")

        # Снова получаем порт для работы
        sock = socket.socket()
        port = get_port(sock)
        sock.close()
        # Возвращаем назад картинку
        sock = socket.socket()
        return_matrix(sock, port, matrix)
        #sys.exit()


def return_matrix(sock, port, matrix):
    logging.info("Start return matrix")
    # Коннектимся к выделенному порту
    sock.connect((server_addr, port))

    sock.send('return'.encode())
    resp = sock.recv(5)
    # TODO а если не ready
    if resp.decode() == 'ready':
        logging.info("...return...")
        send_matrix(sock, matrix)


def get_port(sock):
    # Коннектимся к основному порту планировщика
    logging.info("Connect to Addr: {}, port: {} for get port".format(scheduler_addr, scheduler_port))
    sock.connect((scheduler_addr, scheduler_port))
    # Хотим выделенный порт для работы
    sock.send('get_port'.encode())
    return int.from_bytes(sock.recv(3), 'big')


def get_matrices(sock, port):
    # Коннектимся к выделенному порту
    logging.info("Connect to Addr: {}, port: {} for get_matrices".format(server_addr, port))
    sock.connect((server_addr, port))
    sock.send('get'.encode())
    logging.info("Start read matrices...")
    
    data = sock.recv(1024)
    # TODO
    if not data or data.decode()=='kill':
        logging.info("Kill client process...")
        sys.exit()
        #while True:
         #   pass

    '''
    while True:
        data = sock.recv(1024)
        if data:
            logging.info(f"SOMETHING THERE {len(data)}")
            break
        else: 
            logging.info("Kill client process...")
            while True:
                pass
    '''
    logging.info("...read...")
    #matrices = json.loads(data.decode())
    matrices = util.read_data(data, sock)
    logging.info("Stop read matrices")
    sock.close()
    return matrices


def multiply(matrices):
    A = matrices['A']
    B = matrices['B']
    try:
        matrix = np.multiply(A, B)
    except:
        logging.error("error while multiplying matrices")
    return matrix


def send_matrix(sock, matrix):
    data = json.dumps({"C": matrix.tolist()})
    sock.sendall(data.encode())


start_job()
