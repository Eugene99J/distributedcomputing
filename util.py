import numpy
import logging
import json

file_log = logging.FileHandler("client.log")
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out),
                    format='[%(asctime)s | %(levelname)s | CLIENT]: %(message)s',
                    datefmt='%m.%d.%Y %H:%M:%S',
                    level=logging.INFO)

def read_data(data, sock):
    print(sock)
    data_len = len(data)
    while True:
        data += sock.recv(16777216) # 134217728
        if data_len == len(data):
            break
        else:
            data_len = len(data)
    arr = json.loads(data.decode())
    return arr
