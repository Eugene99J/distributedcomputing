# Планировщик - основной связующий элемент. Знает информацию о свободных и использующихся портах, а так же какие клиенты
# их используют
# TODO сделать мапу для восстановления после сбоя ноды
import argparse
import sys
import logging
import os
import socket
import server2
import multiprocessing

file_log = logging.FileHandler("scheduler.log")
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out),
                    format='[%(asctime)s | %(levelname)s | SCHEDULER]: %(message)s',
                    datefmt='%m.%d.%Y %H:%M:%S',
                    level=logging.INFO)

# TODO сделать массив
manager = multiprocessing.Manager()
ports = []
free_ports = manager.list()
# информация, на каком портц какой клиент сидит
# TODO Manager
port_client = {}
condition = multiprocessing.Condition()


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='path to file with hosts')
    parser.add_argument("-c", "--config", help='path to config file')
    parser.add_argument("-p", "--port", help='count of ports in pull', default=6)
    return parser


def create_workers(hosts: str, config: str):
    try:
        logging.info("Create workers...")
        host_file = open(hosts)
        config_file = open(config)
        path_to_client_file = config_file.readline().replace("\n", "")
        for line in host_file:
            os.system('scp client2.py {}:distributedcomputing/ &'.format(line.replace('\n', '')))
            os.system('scp util.py {}:distributedcomputing/ &'.format(line.replace('\n', '')))
            os.system('ssh {} "python3 {}" &'.format(line.replace('\n', ''), path_to_client_file))
            logging.info("{} created!".format(line.replace('\n', '')))
    except:
        logging.error("error while create workers")


# вечное прослушаваие порта в одном экземпляре, но в отдельном процессе
def main_port_listen(data, conn, addr, hosts, config):
    global free_ports
    if data.decode() == 'get_port':
        # отдаем любой свободный порт, если он есть
        condition.acquire()
        while True:
            if len(free_ports) > 0:
                free_port = free_ports[0]
                logging.info("Send port for " + str(addr))
                port_client[free_port] = addr
                free_ports.remove(free_port)
                conn.send(free_port.to_bytes(3, 'big'))
                break
            else:
                # если нет свободных портов, ждем сообщения о поступлении
                condition.wait()
        condition.release()


def server_port_listen(data, conn, addr, hosts, config):
    global free_ports
    # сигнал от сервера, что процессы на портах создались,
    # можно запускать рабочих
    if data.decode() == 'port_created':
        create_workers(hosts, config)

    if data.decode() == 'return_port':
        conn.sendall('ready'.encode())
        port_number = conn.recv(4)
        condition.acquire()
        free_ports.append(int.from_bytes(port_number, 'big'))
        logging.info("Count of free ports {}".format(len(free_ports)))
        # рассылаем сообщение о поступлении нового свободного порта
        condition.notify()
        condition.release()


def port_listen(func, main_port, hosts, config):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('', main_port))
    sock.listen()
    while True:
        conn, addr = sock.accept()
        logging.info("Connected: " + str(addr))
        while True:
            data = conn.recv(1024)

            logging.info("Command " + data.decode() + " from " + str(addr))

            if not data:
                break

            func(data, conn, addr, hosts, config)

        logging.info("Connection with " + str(addr) + " will be closed")
        conn.close()


# TODO сделать местный kill
def start_scheduler(hosts: str, config: str):
    # последовательность важна
    main_port_listen_process = multiprocessing.Process(target=port_listen,
                                                       args=(main_port_listen, 9090, hosts, config))
    main_port_listen_process.start()
    server_port_listen_process = multiprocessing.Process(target=port_listen,
                                                         args=(server_port_listen, 9091, hosts, config))
    server_port_listen_process.start()
    server2.start_server(ports)
    # рабочие запускаются после сигнала сервера


if __name__ == '__main__':
    # global free_ports
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])
    # инициализируем возможные порты
    start_port = 9093
    for i in range(0, args.port):
        ports.append(start_port)
        # изначально все порты свободны
        free_ports.append(start_port)
        start_port += 1
    start_scheduler(args.host, args.config)
