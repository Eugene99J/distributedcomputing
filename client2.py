# Рабочий - забирает картинки с сервера, распознает их и отдает обратно
import os
import socket
import dlib
import logging
import util
import json
import sys
import numpy as np
from scipy.signal import convolve2d

file_log = logging.FileHandler("client.log")
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out),
                    format='[%(asctime)s | %(levelname)s | CLIENT]: %(message)s',
                    datefmt='%m.%d.%Y %H:%M:%S',
                    level=logging.INFO)

# main_port
scheduler_port = 9090
scheduler_addr = '192.168.88.252'
server_addr = '192.168.88.252'

kernel = []

def generate_kernel():
    global kernel
    for i in range(10):
        row = []
        for j in range(10):
            row.append(np.random.rand())
        kernel.append(row)
        
# Рабочие не спят, работают 24/7
# TODO сделать местный kill
def start_job():

    generate_kernel()

    while True:
        sock = socket.socket()
        # Хотим выделенный порт для работы
        port = get_port(sock)
        sock.close()

        # Хотим матрицы
        sock = socket.socket()
        matrices = get_matrices(sock, port)

        # Распознаем
        logging.info("Start convolute...")
        matrix = convolute(matrices)
        logging.info("Convoluted!")

        # Снова получаем порт для работы
        sock = socket.socket()
        port = get_port(sock)
        sock.close()
        # Возвращаем назад картинку
        sock = socket.socket()
        return_matrix(sock, port, matrix)
        sock.close()


def return_matrix(sock, port, matrix):
    logging.info("Start return matrix")
    # Коннектимся к выделенному порту
    sock.connect((server_addr, port))

    sock.send('return'.encode())
    
    resp = sock.recv(5)
    # TODO а если не go
    if resp.decode() == 'go':
        sock.sendall(socket.gethostname().encode())
        #data = json.dumps({"C": kernel}).encode()
        mat = np.matrix(matrix)
        filename = 'matrix'+socket.gethostname()+'.txt'
        with open(filename,'wb') as f:
            for line in mat:
                np.savetxt(f, line, fmt='%.2f')
        logging.info("...return...")
        file_size = os.path.getsize('/home/rock64/'+filename)
        sock.sendall(str(file_size).encode())
        os.system('scp matrix{}.txt {}:distributedcomputing/ &'.format(socket.gethostname(), '192.168.88.252'))
        #sock.sendall(data)
        logging.info("ConvMatrix sent from client")
        #logging.info("Returned arr {}".format(len(data)))


def get_port(sock):
    # Коннектимся к основному порту планировщика
    logging.info("Connect to Addr: {}, port: {} for get port".format(scheduler_addr, scheduler_port))
    sock.connect((scheduler_addr, scheduler_port))
    # Хотим выделенный порт для работы
    sock.send('get_port'.encode())
    return int.from_bytes(sock.recv(3), 'big')


def get_matrices(sock, port):
    # Коннектимся к выделенному порту
    logging.info("Connect to Addr: {}, port: {} for get_matrices".format(server_addr, port))
    sock.connect((server_addr, port))
    sock.send('get'.encode())
    logging.info("Start read matrices...")
    
    data = sock.recv(1024)
    # TODO
    if not data or data.decode()=='kill':
        logging.info("Kill client process...")
        sys.exit()

    logging.info("...read...")
    matrices = util.read_data(data, sock)
    logging.info("Stop read matrices")
    sock.close()
    return matrices


def convolute(matrices):
    global kernel
    A = matrices['A']   
    B = matrices['B']
    conv_a = convolve2d(A, kernel, 'same')
    conv_b = convolve2d(B, kernel, 'same')
    matrix = np.concatenate((conv_a, conv_b))

    return matrix

start_job()
