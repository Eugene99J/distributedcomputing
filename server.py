# Сервер - держатель общих данных, он их отдает и сохраняет процессобезопасно
import os
import cv2
import mxnet as mx
import logging
import util
import socketserver
import socket
import multiprocessing
import json
import numpy as np
import time
import sys

file_log = logging.FileHandler("server.log")
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out),
                    format='[%(asctime)s | %(levelname)s | SERVER]: %(message)s',
                    datefmt='%m.%d.%Y %H:%M:%S',
                    level=logging.INFO)
kv = mx.kv.create('local')

ports = []

scheduler_port = 9091


# общие данные
manager = multiprocessing.Manager()
# TODO fix it
result = manager.list([0])
# TODO fix it
free_matrices = manager.list([0])

# для каждой операции свой лок, т.к. общие данные не пересакаются в разных операциях
returnLock = multiprocessing.Lock()
getLock = multiprocessing.Lock()

t1 = 0

def generate_matrices():
    matrices = []
    try:
        for i in range(3):
            A = np.round(np.random.rand(1000, 1000)*10)
            B = np.round(np.random.rand(1000, 1000)*10)
            list = [A, B]
            matrices.append(list)
    except:
        logging.error("error while generate matrices")
    print('Matrices generated')
    return matrices


def start_server(_ports):
    # записываем порты
    global free_matrices
    global matrices
    global t1
    ports = _ports
    matrices = manager.list(generate_matrices())
    t1 = time.perf_counter()
    free_matrices[0] = len(matrices)
    listen(ports)


# основной обработчик задач от клиентов
def listen_process(ip, port):
    global t1
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((ip, port))
    logging.info("Listening..")
    sock.listen()
    while True:
        conn, addr = sock.accept()
        logging.info("Connected: " + str(addr))
        while True:
            global result
            global lock

            data = conn.recv(1024)

            logging.info("SERVER Command " + data.decode() + " from " + str(addr))

            if not data:
                logging.error("Connection with " + str(addr) + " will be closed. Data not found")
                break

            if data.decode() == 'get':
                give_matrix(conn)
                break

            if data.decode() == 'return':
                get_matrix(conn)
                break

        logging.info("Connection with " + str(addr) + " will be closed")
        close_port(port, conn)

        if free_matrices[0] == -3:
            logging.info("execution time {}".format(time.perf_counter() - t1))
            sys.exit()


def close_port(port, conn):
    port = port
    conn.close()
    sock = socket.socket()
    sock.connect(('', scheduler_port))
    sock.send('return_port'.encode())
    resp = sock.recv(5)
    # TODO а если не ready
    logging.info("Scheduler answer: {}".format(resp.decode()))
    if resp.decode() == 'ready':
        sock.send(port.to_bytes(3, 'big'))
    sock.close()


# запускаем на каждом порту в пуле прослушивание. Каждый порт слушает в отдельном процессе
def listen(ports):
    logging.info("Listening...")
    process = []
    for i in ports:
        main_process = multiprocessing.Process(target=listen_process, args=('', i))
        main_process.start()
        process.append(main_process)
        logging.info("Started processing on port {} ".format(i))

    #TODO вынести в функцию или класс

    # сообщаем планировщику, что все процессы запустились и можно продолжать работу
    sock = socket.socket()
    sock.connect(('', scheduler_port))
    sock.send('port_created'.encode())
    sock.close()
    for i in process:
        # ожидаем завершения процессов, иначе общие данные пропадут (Manager умирает при убивании основного процесса)
        i.join()


def give_matrix(conn):
    global free_matrices

    logging.info("Start processing give...")
    logging.info("free_matrices[0] {}".format(free_matrices[0]))
    # вешаем лок только на общие данные
    returnLock.acquire()
    if free_matrices[0] <= 0:
        # задач больше нет, завершаем клиента
        logging.info("Killing client process...")
        conn.sendall('kill'.encode())
        free_matrices[0] -= 1
        # снимаем блокировку, чтобы завершились остальные клиенты
        returnLock.release()
        return
    free_matrices[0] -= 1
    returnLock.release()

    logging.info("Send matrix...")
    m = matrices[free_matrices[0]]
    data = json.dumps({"A": m[0].tolist(), "B": m[1].tolist()})
    logging.info("Len arr {}".format(len(data)))
    data = data.encode()
    conn.sendall(data)
    mat = json.loads(data.decode())
    logging.info("Stop processing give")
    return


def get_matrix(conn):
    logging.info("Start processing return...")
    conn.sendall('ready'.encode())
    matrix = ''.encode()
    matrix = util.read_data(matrix, conn)
    matrix = matrix['C']
    # вешаем лок только на общие данные
    getLock.acquire()
    matrix_name = str(result[0])
    result[0] += 1
    getLock.release()

    mat = np.matrix(matrix)
    '''
    with open(matrix_name, 'wb') as f:
        for line in mat:
            np.savetxt(f, line, fmt='%.2f')
    '''
    logging.info("Computing success! Matrix: " + str(len(matrix)))
    logging.info("Stop processing return")
